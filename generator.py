#Imports
from pathlib import Path
from PIL import Image
import random
#import sqlite3

#Asset Folder Paths
assetFolders = [Path("bg"), Path("bases"), Path("eyes"), Path("mouth"), Path("headwear")]

#Assets
allAssetsByCategory = []
for folder in assetFolders:
    assetsInCategory = [asset for asset in folder.iterdir() if asset.is_file()]
    allAssetsByCategory.append(assetsInCategory)

class SloppySteve:
    def __init__(self, traits):
        self.traits = traits

    @staticmethod
    def generate():
        generatedTraits = []
        for assetType in allAssetsByCategory:
            generatedTraits.append(random.choice(assetType));
        return SloppySteve(generatedTraits)

    @staticmethod
    def generateN(n):
        steveList = []
        for i in range(n):
            newlyGenerated = SloppySteve.generate()
            steveList.append(newlyGenerated)
        return steveList
            

    def saveToImage(self):
        resultImg = Image.new("RGBA", (1000, 1000))
        for layer in self.traits:
            resultImg = Image.alpha_composite(resultImg, Image.open(layer).convert("RGBA"))            
        resultImg.save("newSteve.png")

    @staticmethod
    def saveNToImages(steveList):
        i = 1
        for steve in steveList:
            resultImg = Image.new("RGBA", (1000, 1000))
            for layer in steve.traits:
                resultImg = Image.alpha_composite(resultImg, Image.open(layer).convert("RGBA"))
            resultImg.save("newSteve_" + str(i) + ".png")
            i += 1
            
